# GitLab JWT Auth Vault to S3 Bucket Upload


Demonstration of the newly added feature of setting Templates in AWS S3 Policies in Hashicorp Vault ([related GitHub issue](https://github.com/hashicorp/vault/issues/9934)).


## Configure JWT and the policies in Vault

```bash
# the AWS credentials that vault is going to use to create temporary accounts
AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
bucket_name="replace-me"
gitlab_project_path="gitlab-group/gitlab-project"

vault secrets enable aws
vault auth enable jwt

vault write aws/config/root access_key="${AWS_ACCESS_KEY_ID}" secret_key="${AWS_SECRET_ACCESS_KEY}" region=us-east-1
# change this if you're using a self-hosted GitLab instance
vault write auth/jwt/config jwks_url="https://gitlab.com/-/jwks"  bound_issuer="gitlab.com"
# verify that a the jwt auth is now showing up
vault auth list
vault write -f auth/jwt/role/gitlab-aws-access - <<EOF
{
  "role_type": "jwt",
  "policies": ["gitlab-aws-access"],
  "token_explicit_max_ttl": 600,
  "user_claim": "user_email",
  "bound_claims": {
    "project_path": "${gitlab_project_path}"
  },
  "claim_mappings": {
    "project_path": "project_path",
    "ref": "ref",
    "ref_type": "ref_type"
  }
}
EOF

vault policy write gitlab-aws-access - <<EOF
path "aws/creds/gitlab-aws-access" {
  capabilities = ["read",  "list"]
}
EOF

# the accessor is needed for accessing the `claim_mappings` after authentication via JWT
accessor="$(vault auth list | awk '/jwt/ { print $3; exit }')"
vault write aws/roles/gitlab-aws-access credential_type=iam_user policy_document_template=true policy_document=- <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "s3:*"
            ],
            "Resource": "arn:aws:s3:::${bucket_name}/{{ identity.entity.aliases.${accessor}.metadata.project_path }}/{{ identity.entity.aliases.${accessor}.metadata.ref_type }}/{{ identity.entity.aliases.${accessor}.metadata.ref }}/*"
        }
    ]
}
EOF
```

### Setup GitLab Pipeline

See [`.gitlab-ci.yml`](./.gitlab-ci.yml).
